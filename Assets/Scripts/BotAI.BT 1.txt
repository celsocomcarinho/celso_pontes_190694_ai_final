//Se ele estiver com menos de 50% da vida, começa a recuperar
tree("Root")
	parallel
		repeat mute tree("Patrol")
		repeat mute tree("Dead")
		repeat mute tree("Recover")

//Se patrol, pode atacar ou andar
tree("Patrol")
	fallback
		tree("Attack")
		//Se tiver vida abaixo de 40, faz wander
		while IsHealthLessThan(40.0)
			fallback
				tree("Wander")

//Se attack, faz a sequência: 
tree("Attack")
	while SeePlayer
		sequence
			//Pega a posição do player
			TargetPlayer
			//Vira o bota para o player
			LookAtTarget
			//Atira
			Fire

//Se vida está abaixo de 0.1, então o bot explode
tree("Dead")
	sequence
		IsHealthLessThan(0.1)
		Explode

//Se wander ele escolhe um destino e vai
tree("Wander")
	sequence
		//escolhe um destino aleatório
		PickRandomDestination
		//move para o destino escolhido
		MoveToDestination

//Se estiver fraco recupera vida
tree("Recover")
	sequence
		IsHealthLessThan(50.0)
		RecoverHealth