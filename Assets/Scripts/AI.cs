﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using Panda;

public class AI : MonoBehaviour
{
    public Transform player;
    public Transform bulletSpawn;
    public Slider healthBar;   
    public GameObject bulletPrefab;
    Vector3 distance;

    NavMeshAgent agent;
    public Vector3 destination; // The movement destination.
    public Vector3 target;      // The position to aim to.
    public float timeSinceLastShot = 0f;
    public float timerBetweenShots = 0.3f;
    float health = 100.0f;
    float rotSpeed = 5.0f;

    float visibleRange = 80.0f;
    float shotRange = 40.0f;

    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        agent.stoppingDistance = shotRange - 5; //for a little buffer
        //InvokeRepeating("UpdateHealth", 5, 0.5f);
        player = GameObject.Find("Player").GetComponent<Transform>();
    }

    void Update()
    {
        Vector3 healthBarPos = Camera.main.WorldToScreenPoint(this.transform.position);
        healthBar.value = (int)health;
        healthBar.transform.position = healthBarPos + new Vector3(0,60,0);
    }


    [Task]
    public bool RecoverHealth()
    {
       //if(health < 100)
        health += 0.2f;
        return true;
    }

    //removido para evitar das balas baterem umas nas outras
    //void OnCollisionEnter(Collision col)
    //{
    //    if(col.gameObject.tag == "bullet")
    //    {
    //        health -= 10;
    //    }
    //}

    //mdança para Trigger para não interferir no sistema de colisão
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "bullet")
        {
            health -= 10;
        }
    }

    //escolhe um destino
    [Task]
    public void PickDestination(int x, int z)
    {
        Vector3 dest = new Vector3(x, 0, z);
        agent.SetDestination(dest);
        Task.current.Succeed();
    }

    //escolhe um destino aleatório - wander
    [Task]
    public void PickRandomDestination()
    {
       Vector3 dest = new Vector3(Random.Range(-100, 100), 0, Random.Range(-100, 100));
       agent.SetDestination(dest);
       Task.current.Succeed();
    }
    
    //move até o destino escolhido
    [Task]
    public void MoveToDestination()
    {
        if (Task.isInspected)
            Task.current.debugInfo = string.Format("t={0:0.00}", Time.time);
        
        //verifica se chegou no destino
        if (agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending)
        {
            Task.current.Succeed();
        }
    }

    //pega a posição do player para atirar
    [Task]
    public void TargetPlayer()
    {
        target = player.transform.position;
        Task.current.Succeed();
    }

    //fogo no rabo do muleke!!
    [Task]
    public bool Fire()
    {
        //adicionei umas paradas para controlar o fire rate
        timeSinceLastShot += 1 * Time.deltaTime; //Update counter
        if (timeSinceLastShot >= timerBetweenShots){ //If it's been long enough to shoot again
            GameObject bullet = (GameObject) Instantiate(bulletPrefab, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
            bullet.GetComponent<Rigidbody>().AddForce(bullet.transform.forward * 2000);
            timeSinceLastShot = 0f;//Reset the timer
            return true;
        }
        return true;
    }

    //vira o bot para olhar na direção do player
    [Task]
    public void LookAtTarget()
    {
        Vector3 direction = target - this.transform.position;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);

        if (Task.isInspected)
            Task.current.debugInfo = string.Format("angle={0}", Vector3.Angle(this.transform.forward, direction));

        if (Vector3.Angle(this.transform.forward, direction) < 5.0f)
        {
            Task.current.Succeed();
        }
    }

    //Verifica se o player está visível.
    [Task]
    bool SeePlayer()
    {
        distance = player.transform.position - this.transform.position;
        RaycastHit hit;
        bool seeWall = false;
        Debug.DrawRay(this.transform.position, distance, Color.red);
        if (Physics.Raycast(this.transform.position, distance, out hit))
        {
            if (hit.collider.gameObject.tag == "wall")
            {
                seeWall = true;
            }
        }

        if (Task.isInspected)
        Task.current.debugInfo = string.Format("wall={0}", seeWall);

        if (distance.magnitude < visibleRange && !seeWall)
            return true;
        else
            return false;
    }

    //Vira o NPC. Utilizado no LookArround.
    //NÃO FUNCIONOU!
    [Task]
    bool Turn(float angle)
    {
        var p = this.transform.position + Quaternion.AngleAxis(angle, Vector3.up) * this.transform.forward;
        target = p;
        return true;
    }

    //Verifica se a vida do bot é menor do que a informada;
    [Task]
    public bool IsHealthLessThan(float health)
    {
        return this.health < health;
    }
    
    //Destroi bot e barra de vida;
    [Task]
    public bool Explode()
    {
        Destroy(healthBar.gameObject);
        Destroy(this.gameObject);
        return true;
    }

    [Task]
	public void FollowPlayer()
    {
		Vector3 dest = new Vector3(player.transform.position.x,player.transform.position.y,player.transform.position.z);
		agent.SetDestination(dest);
		Task.current.Succeed();
	}
    
}