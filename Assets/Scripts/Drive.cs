﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Drive : MonoBehaviour {

	Rigidbody rb;
    public float speed = 20.0F;
    float rotationSpeed = 120.0F;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public Slider healthBar;  //barra de vida
    float health = 100.0f; 
    public Transform spawnPoint; //ponto de respawn

    void Start() {
        rb = GetComponent<Rigidbody>(); //rb para movimento com AddForce
    }

    //COMENTAR PARA PRÒXIMO EXERCÍCIO!!!
    void Update() {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        //translation *= Time.deltaTime; //para mover com Translate
        rotation *= Time.deltaTime;
        //transform.Translate(0, 0, translation); //para mover com Translate
        transform.Rotate(0, rotation, 0);
        rb.AddForce(this.transform.forward * translation); //para mover com RigidBody

        //fogo no muleke!!
        if(Input.GetKeyDown("space"))
        {
            GameObject bullet = (GameObject) Instantiate(bulletPrefab, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
            bullet.GetComponent<Rigidbody>().AddForce(bullet.transform.forward*2000);
        }
        
        //atualiza barra de vida
        UpdateHealth();
    }

    //levou bala? Toma 10!
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "bullet")
        {
            health -= 10;
        }
    }

    //atualiza a barra de vida
    public void UpdateHealth(){
        Vector3 healthBarPos = Camera.main.WorldToScreenPoint(this.transform.position);
        healthBar.value = (int)health;
        healthBar.transform.position = healthBarPos + new Vector3(0,60,0);

        //morreu? respwan.
        if (health <= 0) Respawn(); 
    }
    
    //restaura vida e teleporta para spawn point position;
    public void Respawn()
    {
        health = 100;
        transform.position = spawnPoint.position;
    }

}
