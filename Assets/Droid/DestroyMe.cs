﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour {

	//void OnCollisionEnter(Collision col)
	//{
	//  Destroy(this.gameObject);
	//}
	private void OnTriggerEnter(Collider other)
	{
        if(other.gameObject.tag != "bullet")
			Destroy(this.gameObject);
	}
}
